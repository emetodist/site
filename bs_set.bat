@echo off
cls
SET st=%time%
echo Set to rhc...

git add .
git commit -a -m %1
git push %2 HEAD

rem call ping 38.38.38.38 -w 4000

call rhc ssh %2 --command "set $TMOUT=3600 ; mysql -u $OPENSHIFT_MYSQL_DB_USERNAME --password=$OPENSHIFT_MYSQL_DB_PASSWORD -h $OPENSHIFT_MYSQL_DB_HOST $OPENSHIFT_APP_NAME < $OPENSHIFT_REPO_DIR/backupDB.sql"

echo --------------------------
echo --- Start  : %st%
echo --- Stop   : %time%

rem --- win
rem git update-index --chmod=+x .openshift/action_hooks/deploy
rem call rhc ssh prd --command "set $TMOUT=3600 ; mysql -u $OPENSHIFT_MYSQL_DB_USERNAME --password=$OPENSHIFT_MYSQL_DB_PASSWORD -h $OPENSHIFT_MYSQL_DB_HOST $OPENSHIFT_APP_NAME < $OPENSHIFT_REPO_DIR/backupDB.sql"



rem --- gear
rem https://forums.openshift.com/solved-kindof-drupal-install-is-drush-andor-uploadprogress-installable
rem I looked through http://drupal.org/project/drush and was able to install drush on my gear (ssh'd onto the gear first) with:
rem $> pear channel-discover pear.drush.org
rem $> pear install drush/drush
rem $> ~/php-5.3/phplib/pear/pear/drush
rem Execute a drush command. Run drush help [command] to view command-specific help.... 
