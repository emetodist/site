@echo off
cls
SET st=%time%
echo Get from rhc...

call rhc ssh uat --command "set $TMOUT=3600 ; rm -f ~/app-root/runtime/repo/backupDB.sql ; mysqldump -h $OPENSHIFT_MYSQL_DB_HOST -u $OPENSHIFT_MYSQL_DB_USERNAME --password=$OPENSHIFT_MYSQL_DB_PASSWORD $OPENSHIFT_APP_NAME > ~/app-root/runtime/repo/backupDB.sql"

call winscp.com /script=bs_get_script_uat.txt

git add .
git commit -a -m "bs_to_prd.bat"
git push prd HEAD

rem call ping 38.38.38.38 -w 4000

call rhc ssh prd --command "set $TMOUT=3600 ; mysql -u $OPENSHIFT_MYSQL_DB_USERNAME --password=$OPENSHIFT_MYSQL_DB_PASSWORD -h $OPENSHIFT_MYSQL_DB_HOST $OPENSHIFT_APP_NAME < $OPENSHIFT_REPO_DIR/backupDB.sql"


echo --------------------------
echo --- Start  : %st%
echo --- Stop   : %time%
