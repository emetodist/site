@echo off
cls
SET st=%time%
echo Get from rhc...

call rhc ssh uat --command "set $TMOUT=3600 ; rm -f ~/app-root/runtime/repo/backupDB.sql ; mysqldump -h $OPENSHIFT_MYSQL_DB_HOST -u $OPENSHIFT_MYSQL_DB_USERNAME --password=$OPENSHIFT_MYSQL_DB_PASSWORD $OPENSHIFT_APP_NAME > ~/app-root/runtime/repo/backupDB.sql"

call winscp.com /script=bs_get_script_uat.txt

echo --------------------------
echo --- Start  : %st%
echo --- Stop   : %time%

